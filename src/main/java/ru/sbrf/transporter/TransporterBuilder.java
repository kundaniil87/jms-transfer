package ru.sbrf.transporter;

import javax.jms.Connection;

public interface TransporterBuilder {
    public TransporterBuilder to(Connection connectionTo, String queue);

    public TransporterBuilder to(Connection connectionTo, String queue, String routeTo);

    public TransporterBuilder from(Connection connectionFrom, String queue);

    public Transporter create();
}
