package ru.sbrf.transporter;

import sun.java2d.pipe.SpanShapeRenderer;

import javax.jms.*;

public class MQConnector {
    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Queue queue;
    private MessageProducer producer;
    private MessageConsumer consumer;
    private Queue replyTo;
    private boolean isStarted;

    public MQConnector(ConnectionFactory connectionFactory, String queueName) throws JMSException {
        this.connectionFactory = connectionFactory;
        init(queueName);
    }

    public MQConnector(Connection connection, String queueName) throws JMSException{
        this.connection = connection;
        init(queueName);
        SimpleLogger.printDebug("Connection to queue " + queueName + " established");
    }

    private void init(String queueName) throws JMSException {
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = session.createQueue(queueName);

        isStarted = false;
    }

    public void setReplyTo(String replyTo) throws JMSException{
        this.replyTo = session.createQueue(replyTo);
        SimpleLogger.printDebug("Connection to replyTO queue " + replyTo + " established");
    }

    public void putMessageToQueue(String msg) throws JMSException {
        Message messageToSend = session.createTextMessage(msg);

        putMessageToQueue(messageToSend);
    }

    public void putMessageToQueue(Message msg) throws JMSException {
        //SimpleLogger.printDebug("In put message to queue " + getQueueName());
        if(producer == null){
            producer = getMessageProducer();
        }
//        SimpleLogger.printDebug("Message producer have been gotten");
//        SimpleLogger.printDebug(replyTo.getQueueName());
//        SimpleLogger.printDebug(msg.getJMSReplyTo().toString());
//        SimpleLogger.printDebug("To String worked good");
//        if(replyTo != null)
//        {
//            try {
//                msg.setJMSReplyTo(replyTo);
//            }
//            catch (Exception e) {
//                SimpleLogger.printDebug(e.getMessage());
//                e.printStackTrace();
//            }
//            SimpleLogger.printDebug(msg.getJMSReplyTo().toString());
//        }
        SimpleLogger.printDebug("ReplyTo have been applied");
        if(producer != null) {
            producer.send(msg);
        }
        SimpleLogger.printDebug("End with put message to queue" + getQueueName());
    }

    public MessageProducer getMessageProducer() throws JMSException{
        if(session !=null) {
            return session.createProducer(queue);
        }
        else {
            return null;
        }
    }

    public Message getMessageFromQueue() throws JMSException {
        if(consumer == null) {
            consumer = getMessageConsumer();
        }
        if(!isStarted){
            connection.start();
            isStarted = true;
        }

        if(consumer != null) {
            SimpleLogger.printDebug("In Receive no wait");
            return consumer.receiveNoWait();
        }
        else {
            return null;
        }
    }



    public MessageConsumer getMessageConsumer() throws JMSException {
        if(session != null) {
            return session.createConsumer(queue);
        }
        else {
            return null;
        }
    }

    public String getQueueName() throws JMSException{
        return queue.getQueueName();
    }

    public void connect() throws JMSException {
        connection.start();
        isStarted = true;
    }

    public void disconnect() throws JMSException{
        try {
            if (isStarted) {
                connection.stop();
            }
        } catch (Exception e){
            SimpleLogger.printError("Error with stopping connection", e);
        }
        finally {
            session.close();
            connection.close();
        }
    }

}
