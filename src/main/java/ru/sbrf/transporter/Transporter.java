package ru.sbrf.transporter;

import javax.jms.Connection;
import javax.jms.JMSException;

public class Transporter {
    private Connection toConnection;
    private String toQueue;
    private MQConnector toMQConnector;
    private String replyTo;

    private Connection fromConnection;
    private String fromQueue;
    private MQConnector fromMQConnector;

    private MQTransfer transferring;

    public void setToConnection(Connection toConnection) {
        this.toConnection = toConnection;
    }

    public void setToQueue(String toQueue) {
        this.toQueue = toQueue;
    }

    public void setReplyTo(String replyTo) { this.replyTo = replyTo; }

    public void setFromConnection(Connection fromConnection) {
        this.fromConnection = fromConnection;
    }

    public void setFromQueue(String fromQueue) {
        this.fromQueue = fromQueue;
    }

    private void createConnection() throws JMSException {
        fromMQConnector = new MQConnector(fromConnection, fromQueue);
        SimpleLogger.printDebug("*** MQConnector created: " + fromQueue);
        fromMQConnector.connect();
        SimpleLogger.printDebug("*** MQConnector From connected: " + fromQueue);

        toMQConnector = new MQConnector(toConnection, toQueue);
        if(replyTo != null){
            toMQConnector.setReplyTo(replyTo);
        }
        SimpleLogger.printDebug("*** Connection created");
    }

    public void startTransfer() throws JMSException {
        createConnection();

        transferring = new MQTransfer(fromMQConnector, toMQConnector);
        SimpleLogger.printDebug("*** Thread Created");
        transferring.start();
        SimpleLogger.printDebug("*** Thread Started");
    }

    public void stopTransfer() throws JMSException {
        try {
            transferring.interrupt();
            transferring.join();
        }
        catch (InterruptedException e){
            SimpleLogger.printError("Error while waiting for interrupting other thread", e);
        }

        fromMQConnector.disconnect();
        toMQConnector.disconnect();
    }
}
