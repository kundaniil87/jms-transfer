package ru.sbrf.transporter;



public class SimpleLogger {
    static private LogLevel logLevel = LogLevel.ERROR;
    static private String preMessage = "";

    static public void setLogLevel(String logLevel){
        if(logLevel != null) {
            try {
                SimpleLogger.logLevel = LogLevel.valueOf(logLevel);
            }
            catch (IllegalArgumentException e){
                SimpleLogger.logLevel = LogLevel.ERROR;
            }
        }
    }

    static public void setPreMessage(String preMessage){
        if(preMessage != null) {
            SimpleLogger.preMessage = preMessage;
        }
    }

    static public void printDebug(String msg){
        if(SimpleLogger.checkLogLevel(LogLevel.DEBUG)){
            System.out.println(SimpleLogger.addPreMessage(msg));
        }
    }

    static public void printInfo(String msg){
        if(SimpleLogger.checkLogLevel(LogLevel.INFO)){
            System.out.println(SimpleLogger.addPreMessage(msg));
        }
    }

    static public void printError(String msg){
        if(SimpleLogger.checkLogLevel(LogLevel.ERROR)){
            System.err.println(SimpleLogger.addPreMessage(msg));
        }
    }

    static public void printError(String msg, Exception e) {
        if(SimpleLogger.checkLogLevel(LogLevel.ERROR)){
            SimpleLogger.printError(SimpleLogger.addPreMessage(msg));
            System.err.println(SimpleLogger.addPreMessage(e.getMessage()));
            e.printStackTrace();
        }
    }

    static private boolean checkLogLevel(LogLevel logLevel){
        switch(logLevel){
            case NONE: if(LogLevel.NONE == SimpleLogger.logLevel){ return true; }
            case ERROR: if(LogLevel.ERROR == SimpleLogger.logLevel){ return true; }
            case INFO: if(LogLevel.INFO == SimpleLogger.logLevel){ return true; }
            case DEBUG: if(LogLevel.DEBUG == SimpleLogger.logLevel) {return true; }
            default: return false;
        }
    }

    static private String addPreMessage(String msg){
        if(!SimpleLogger.preMessage.equals("")) {
            return SimpleLogger.preMessage + " " + msg;
        } else {
            return msg;
        }
    }
}
