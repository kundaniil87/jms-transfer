package ru.sbrf.transporter;

import javax.jms.Connection;

public class TransporterBuilderImpl implements TransporterBuilder {
    private Transporter transporter;

    public TransporterBuilderImpl() {
        transporter = new Transporter();
    }

    public TransporterBuilder to(Connection toConnection, String queue){
        transporter.setToConnection(toConnection);
        transporter.setToQueue(queue);

        return this;
    }

    public TransporterBuilder to(Connection toConnection, String queue, String replyTo){
        to(toConnection, queue);
        transporter.setReplyTo(replyTo);

        return this;
    }

    public TransporterBuilder from(Connection connectionFrom, String queue) {
        transporter.setFromConnection(connectionFrom);
        transporter.setFromQueue(queue);

        return this;
    }

    public Transporter create(){

        return transporter;
    }
}
