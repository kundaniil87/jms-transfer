package ru.sbrf.transporter;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.logging.impl.SimpleLog;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MainServlet implements ServletContextListener {
    private Transporter fromOurToExternal;
    private Transporter fromExternalToOur;

    public void contextDestroyed(ServletContextEvent evn){
        SimpleLogger.printInfo("*** Stop transfer");
        try {
            if (fromOurToExternal != null) {
                fromOurToExternal.stopTransfer( );
            }
            if(fromExternalToOur != null){
                fromExternalToOur.stopTransfer();
            }
            SimpleLogger.printInfo("*** Transferring have been stopped");
        }
        catch (Exception e){
            SimpleLogger.printError("Error while closing transfer connection:", e);
        }
    }

    public void contextInitialized(ServletContextEvent evn) {
        try {
            SimpleLogger.setLogLevel("INFO");
            SimpleLogger.setPreMessage("[Transfer To ECM]:");

            SimpleLogger.printInfo("*** Servlet context loaded");
            String jndiNameForOurConnectionFactory = "ExternalIntegrationFactory";
            String urlForConnection = "tcp://127.0.0.1:61616";
            Context context = new InitialContext();
            SimpleLogger.printDebug("*** Context Path: " + context.getNameInNamespace());
            ConnectionFactory our = (ConnectionFactory)context.lookup(jndiNameForOurConnectionFactory);
            SimpleLogger.printDebug("*** Found connection factory for JNDI: " + our.toString());

            ConnectionFactory external = new ActiveMQConnectionFactory("admin","admin",urlForConnection);
            SimpleLogger.printDebug("*** Found connection factory for ActiveMQ: " + external.toString());

            if ((our != null) && (external != null)) {
                SimpleLogger.printDebug("*** Start to create connections");
                Connection ourConnection = our.createConnection();
                Connection externalConnection = external.createConnection();

                fromOurToExternal = new TransporterBuilderImpl().from(our.createConnection(), "UVZ.TO.ECM")
                        .to(external.createConnection(), "ANY.TO.ECM")
                        .create();

                SimpleLogger.printDebug("*** First connection created");
                fromExternalToOur = new TransporterBuilderImpl().from(external.createConnection(), "ECM.TO.UVZ")
                        .to(our.createConnection(), "ECM.TO.UVZ")
                        .create();

                SimpleLogger.printDebug("*** Connections created");

                fromOurToExternal.startTransfer();
                fromExternalToOur.startTransfer();

                SimpleLogger.printInfo("*** Connections established");

                SimpleLogger.printDebug("*** Connection to queues have been initialized");
            }
        }
        catch(Exception e){
            SimpleLogger.printError("*** Error with creating and execution transporters", e);
        }
    }
}
