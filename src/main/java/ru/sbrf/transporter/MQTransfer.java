package ru.sbrf.transporter;

import sun.java2d.pipe.SpanShapeRenderer;

import javax.jms.*;
import java.io.InterruptedIOException;

public class MQTransfer extends Thread {
    private MQConnector from;
    private MQConnector to;
    private boolean working;

    public MQTransfer(MQConnector from, MQConnector to){
        this.from = from;
        this.to = to;

        working = true;
    }

    public synchronized void stopExecuting() {
        working = false;
    }

    public void run(){
        while(working){
            try {
                //SimpleLogger.printDebug("Waiting for message");
                Message messageForTransfer = from.getMessageFromQueue();
                if(messageForTransfer != null) {
                    SimpleLogger.printDebug("Start to send message");
                    to.putMessageToQueue(messageForTransfer);
                    SimpleLogger.printInfo("Message sent:");
                    SimpleLogger.printInfo(messageForTransfer.getJMSMessageID());
                    SimpleLogger.printInfo("CorrelationID: " + messageForTransfer.getJMSCorrelationID());

                    SimpleLogger.printInfo("From: " + from.getQueueName() + " To: " + to.getQueueName());
                }
                Thread.sleep(1000);
            }
            catch (JMSException e){
                SimpleLogger.printError("*** Error with JMS", e);
                working = false;
            }
            catch (InterruptedException e){
                SimpleLogger.printDebug("*** Interrupting transferring thread");
                SimpleLogger.printDebug(e.getMessage());
                working = false;
            }
            catch (Exception e){
                SimpleLogger.printError("*** UNKNOWN EXCEPTION", e);
                working = false;
            }
        }
        SimpleLogger.printDebug("*** Thread successful stopped");
    }
}
